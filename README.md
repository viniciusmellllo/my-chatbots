# README #

To run this SPA just do a "npm install" and after a "ng serve -o".

### What is this repository for? ###

Challenge: Smart contact list

### What are the functionalities of this SPA? ###

* Add a contact in the list of favorites;
* Allow the user to change the way of viewing;
* Allow the user to change the alphabetical ordering;
* Allow the user to change the ordering by date;
* Allow the user to search for contacts' names;
* When clicking on a contact, the user must be redirected to a screen containing the details of that contact that was clicked.
