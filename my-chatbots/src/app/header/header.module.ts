import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header.component';

@NgModule({
    declarations: [
        HeaderComponent
    ],
    imports: [
        RouterModule
    ],
    providers: [],
    bootstrap: [],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule { }
