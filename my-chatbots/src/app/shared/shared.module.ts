import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatGridListModule, MatInputModule, MatListModule } from '@angular/material';
import { MenuComponent } from './menu/menu.component';

@NgModule({
    declarations: [
        MenuComponent
    ],
    imports: [
        FormsModule,
        MatButtonModule,
        MatGridListModule,
        MatInputModule,
        MatListModule
    ],
    providers: [],
    bootstrap: [],
    exports: [
        MenuComponent,
        FormsModule,
        MatButtonModule,
        MatGridListModule,
        MatInputModule,
        MatListModule
    ]
})
export class SharedModule { }
