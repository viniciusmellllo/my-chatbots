import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BotsService } from 'src/app/bots.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  searchText: String;

  constructor(
    private botsService: BotsService,
    private router: Router
  ) { }

  ngOnInit() { }

  search() {
    this.botsService.searchAction(this.searchText);
  }

  orderByName(orderByName: boolean) {
    this.botsService.orderByNameAction(orderByName);
  }

  orderByCreation(orderByCreation: boolean) {
    this.botsService.orderByCreationAction(orderByCreation);
  }

  cardsView() {
    this.router.navigate(['/cards']);
  }

  listView() {
    this.router.navigate(['/list']);
  }

}
