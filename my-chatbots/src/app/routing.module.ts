import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsComponent } from './cards/cards.component';
import { ListComponent } from './list/list.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
    {
        path: '',
        component: CardsComponent
    },
    {
        path: 'cards',
        component: CardsComponent
    },
    {
        path: 'list',
        component: ListComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'profile/:shortName',
        component: ProfileComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })
    ],
    exports: [RouterModule]
})
export class RoutingModule { }
