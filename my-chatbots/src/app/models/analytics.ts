import { User } from './user';
import { Message } from './message';

export class Analytics {
    user: User;
    message: Message;
}
