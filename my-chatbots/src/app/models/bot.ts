import { Analytics } from './analytics';

export class Bot {
    shortName: String;
    name: String;
    description: String;
    image: String;
    template: String;
    created: Date;
    updated: Date;
    plan: String;
    culture: String;

    favorite: Boolean;

    analytics: Analytics;
}
