import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BotsService } from '../bots.service';
import { SharedModule } from '../shared/shared.module';
import { CardsComponent } from './cards.component';

@NgModule({
    declarations: [
        CardsComponent
    ],
    imports: [
        RouterModule,
        SharedModule,
        CommonModule
    ],
    providers: [
        BotsService
    ],
    bootstrap: [],
    exports: [
        CardsComponent
    ]
})
export class CardsModule { }
