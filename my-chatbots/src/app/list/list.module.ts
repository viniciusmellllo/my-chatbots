import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BotsService } from '../bots.service';
import { SharedModule } from '../shared/shared.module';
import { ListComponent } from './list.component';

@NgModule({
    declarations: [
        ListComponent
    ],
    imports: [
        RouterModule,
        SharedModule,
        CommonModule
    ],
    providers: [
        BotsService
    ],
    bootstrap: [],
    exports: [
        ListComponent
    ]
})
export class ListModule { }
