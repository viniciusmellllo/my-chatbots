import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BotsService } from '../bots.service';
import { Bot } from '../models/bot';

declare var require: any;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  bots: Bot[];
  favoriteBots: Bot[];
  unfavoritedBots: Bot[];

  searchSub: Subscription;
  orderByNameSub: Subscription;
  orderByCreationSub: Subscription;

  constructor(
    private botsService: BotsService,
    private router: Router
  ) {
    this.searchSub = botsService.search$.subscribe(
      search => {
        if (search && this.bots && this.bots.length > 0) {
          this.searchFilter(search);
        } else {
          this.fillLists();
        }
      });

    this.orderByNameSub = botsService.orderByName$.subscribe(
      orderByName => {
        if (orderByName && this.bots && this.bots.length > 0) {
          this.bots.sort((x, y) => x.name > y.name ? 1 : -1);
          this.fillLists();
        }
      });

    this.orderByCreationSub = botsService.orderByCreation$.subscribe(
      orderByCreation => {
        if (orderByCreation && this.bots && this.bots.length > 0) {
          this.bots.sort((x, y) => x.created > y.created ? 1 : -1);
          this.fillLists();
        }
      });
  }

  ngOnInit() {
    const botsFile = require('../../assets/resources/data.json');

    if (botsFile) {
      this.botsService.setBots(botsFile);
      this.bots = botsFile;
      this.fillLists();
    }
  }

  fillLists() {
    this.favoriteBots = this.bots.filter(x => (x.favorite && x.favorite === true));
    this.unfavoritedBots = this.bots.filter(x => (!x.favorite || x.favorite === false));
  }

  setFavorited(bot: Bot, favorited: boolean) {
    event.stopPropagation();
    bot.favorite = favorited;
    this.fillLists();
  }

  searchFilter(search: any) {
    this.favoriteBots = this.favoriteBots.filter(x => x.name.toLowerCase().includes(search.toLowerCase()));
    this.unfavoritedBots = this.unfavoritedBots.filter(x => x.name.toLowerCase().includes(search.toLowerCase()));
  }

  botDetails(bot: Bot) {
    this.router.navigate(['/profile/' + bot.shortName]);
  }

  ngOnDestroy() {
    if (this.searchSub) {
      this.searchSub.unsubscribe();
    }
    if (this.orderByNameSub) {
      this.orderByNameSub.unsubscribe();
    }
    if (this.orderByCreationSub) {
      this.orderByCreationSub.unsubscribe();
    }
  }

}
