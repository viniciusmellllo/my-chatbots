import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bot } from '../models/bot';

declare var require: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  botShortName: String;
  bot: Bot;
  bots: Bot[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.botShortName = p['shortName'];
    });
    if (!this.botShortName) {
      this.router.navigate(['']);
    }

    const project = require('../../assets/resources/data.json');
    if ((!this.bots || this.bots.length === 0) && project) {
      this.bots = project;
    }

    this.fillBotData();
  }

  fillBotData() {
    this.bot = this.bots.find(x => x.shortName === this.botShortName);

    if (!this.bot) {
      this.router.navigate(['']);
    }
  }

}
