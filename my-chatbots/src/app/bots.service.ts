import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Bot } from './models/bot';

@Injectable()
export class BotsService {

    private bots = new Subject<Array<Bot>>();
    private search = new Subject<String>();
    private orderByName = new Subject<boolean>();
    private orderByCreation = new Subject<boolean>();

    bots$ = this.bots.asObservable();
    search$ = this.search.asObservable();
    orderByName$ = this.orderByName.asObservable();
    orderByCreation$ = this.orderByCreation.asObservable();

    setBots(bots: Array<Bot>) {
        this.bots.next(bots);
    }

    searchAction(search: String) {
        this.search.next(search);
    }

    orderByNameAction(orderByName: boolean) {
        this.orderByName.next(orderByName);
    }

    orderByCreationAction(orderByCreation: boolean) {
        this.orderByCreation.next(orderByCreation);
    }
}
